## CV: peter lu

### Summary
I am an independent game designer and artist. my practice includes ceramics, game design, carpentry, code, fermentation, math, mycology and feminist social activism. I work towards the deconstruction of the imperialist capitalist patriarchal system. To this end, my work tackles the personal and the political. I believe to influence the world, I must continuously understand how the world influences me. Below are selected works from my site that demonstrate this process:

- [Perfect Woman](http://pdlla.org/posts/portfolio/games/perfect_woman.html)
- [Mushroom #1](http://pdlla.org/posts/portfolio/art/mushrooms/mushroom_throne.html)
- [Cave](http://pdlla.org/posts/portfolio/games/cave.html)
- [Clay e-mails](http://pdlla.org/posts/portfolio/art/ceramics/clay_emails.html)
- [Mushroom #2](http://pdlla.org/posts/portfolio/art/mushrooms/freshman_mushroom_project.html)
- [Poops Pots and Pottery](http://pdlla.org/posts/portfolio/art/ceramics/poops_pots_and_pottery.html)
- [Epoch 5: Paradigm Shift](http://pdlla.org/posts/portfolio/games/epoch5.html)

### Education
- BA, Pure Mathematics, UCLA (2011)
- MA, Mathematics, UCLA (2011)
- MFA, in progress, [Design | Media Arts (2016)](http://dma.ucla.edu/grad/profiles/?ID=163)

### Skills
- game design, production and development
- C++, Haskell, Unity3D, C#, Go, Java, Python, Javascript, OpenGL
- finite elements and finite difference, numeric linear algebra, collision detection algorithms, blockchain
- electronics, sewing, ceramics, woodworking, mycology, fermentation, yoga (CYT 200)

### Links
- [website](http://pdlla.org/)
- [github](https://github.com/pdlla)
- [online CV](http://pdlla.org/cv.html)

### Professional Experience
- *May 2020 - Present:*
	- Principle Engineer at Roblox
	
- *May 2018 - December 2019:*
	- Product and Research Specialist at [Thunder Token](https://www.thundertoken.com/). Go.
	- Consensus protocol developer
	- Primary author of our [whitepaper and blockchain consensus series](http://pdlla.org/posts/writing/2019-12-10_blockchain_consensus.html)

- *February 2017 - March 2018:*
	- Fighting for equality in the games industry as a Senior Gameplay Engineer at [Super Evil Megacorp](http://www.superevilmegacorp.com/). C++.

- *August 2016 - January 2017:*
	- [Blender](www.blender.org) contributor by night. My list of contributions can be seen [here](https://developer.blender.org/differential/query/biusJjrFLIIO/).

- *Oct 2012 - August 2016:*
	- Lead developer and  co-creator for [Perfect Woman](http://www.perfectwomangame.com/). Perfect Woman is an award winning, innovative and socially aware motion controlled (Kinect) dance game for Xbox One. Created by myself and Lea Schönfelder, Perfect Woman has been showed in numerous public places to critical acclaim.

- *Oct 2012 - January 2015:*
	- Independent Contractor for Zahner. I helped design and implement a Unity3D web app to customize different Zahner products. App is live at [here](http://www.shopfloorapp.com/).

- *June 2011 - May 2012:*
	- 77-pieces: Software Engineer R&D. 77-pieces develops clothing simulation software for applications in all levels of the fashion industry. Designed, implemented, and maintained fabric collision and simulation code as well as general programming when needed. Used finite elements for fabric simulation and Bridson collision model with a fast reconstructing bounding volume hierarchy. Backend C++ frontend Unity. Content generated from my code can be seen [here](/posts/hidden/77pieces.html).

- *August 2009 - February 2011:*
	- Developer for [RobotBear](http://robotbear.pdlla.org/). RobotBear is a mobile and desktop cross platform games collective. In addition to games programming, I co-developed our technology in C++ to allow cross-platform release of all our products.

- *July 2010 - December 2010:*
	- Developer for Eddo Stern's [Darkgame](http://eddostern.com/works/darkgame-3/) and UCLA Game Lab’s [Flatland](http://flatland.games.ucla.edu/) (directed by Eddo Stern). Darkgame is a multiplayer game about sensory deprivation programmed in Unity 3D and C# with a novel physics based menu [UI](http://vimeo.com/52597481).

- *August 2009 - December 2016:*
	- Resident and volunteer for the [UCLA Game Lab](http://games.ucla.edu/). The UCLA Game Lab is an independent lab focused on researching artistic and expressive potential of games through innovative approaches in game design in both content and form. We harbour a diverse group of talented and motivated individuals all interested in exploring games in this way.

### Teaching Experience
- *Spring 2014 - present:*
	- Adjunct Professor at USC, Interactive Media and Games. I teach the [Immersive](http://pdlla.org/posts/teaching/USC_486_F14.html) and [Alternative Control](http://pdlla.org/posts/teaching/USC_486_F16.html) Workshop classes.

- *Fall 2014 - Spring 2016:*
	- TA at UCLA, Design | Media Arts. I primarily TA for Eddo Stern's [game design class](http://pdlla.org/posts/teaching/UCLA_DMA157.html).
