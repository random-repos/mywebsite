---
title: About
date: September 22, 2014
---

***2/21/2020 - MOTD:***
```
yooooooogggggaaaaaaaaaa
```

***1/1/2025 - Change Log:***
```
-randomize image size in gallery
-reorganize some stuff in galleries
-add a lot of new galleries
```


---------------------

I am a game developer and mushroom artist. I am interested in finding new ways to disrupt and change the oppressive social order. You can see some words about and pictures of my work on this site.

I finally made this website which is about 10 years overdue at this point. Better late than never. The nice part about making a website 10 years overdue is that I have 10 years worth of content to add to the site. I finally settled on a style built on web technology over 20 years old. Maybe you care to [read a little more about it](http://contemporary-home-computing.org/prof-dr-style/).

You can follow me on [twitter](https://twitter.com/_kitchen) and [github](https://github.com/pdlla).

This site was built with HTML and [hakyll](http://jaspervdj.be/hakyll/).

---------------------

#### HISTORY

***12/14/2024 - Change Log:***
```
-add script to resize images in gallery
```

***7/23/2023 - Change Log:***
```
-add MOBA idea post
-update some pages
-add miscwood1
-add more animal pics
```

***9/2/2022 - Change Log:***
```
-add new blog post for DE&I scenario role playing
-add new blog post for DE&I community guidelines
-add new blog post for DE&I meeting moderation guide
-downgrade clay e-mails to clay hand written letters
-chicken house :O, not yet maybe next time
-tiny tools? Also not yet 😱
-more animal pics
-updated CV
```



***6/9/2021 - Change Log:***
```
-add blog post for 2021 show reviews
-update blog post for inclusive language in code
-add new blog post for inclusive naming
-added more animals
```

***6/21/2020 - Change Log:***
```
-created page for shaping culture through play
-created blog article for world centric blockchain games
-removed expired/parked domains linked in USC 486 S16 :(
-updated CV
```

***3/24/2020 - Change Log:***
```
-updated world-centric game design essay
```

***2/21/2020 - Change Log:***
```
-fixed writing section
-upgraded to newest version of hakyll with stack... Now that I actually know how to program haskell...
-added oss page
-updated CV
```

***12/10/2019 - Change Log:***
```
-added ceramics centering guide to misc art 2
-added zip file of all crypto stickers in misc art 2
-added bytecraft image to misc art 2
-added misc ceramics 2
-added blockchain consensus to writing
-removing marginalized folx who have been influential in my life from about page. They're all doing well and we all need to move on
```

***03/30/2019 - Change Log:***
```
-added bitcoin stickers to misc art 2018
-added pixelart page
-added hidden pirate sticker page 😱
```

***08/28/2018 - Change Log:***
```
-history section in about page reorganized
-added bytecraft to misc art
-minor changes to about page
```

***6/27/2018 - Change Log:***
```
-updated source links for cave and roulette
-other minor fixes
-updated CBG essay
-updated CV so as not to elicit a particular passionate or fearful response
```

***5/17/2018 - MOTD:***
```Happily unemployed and contemplating my next adventure :). Writing lots of Haskell and doing lots of yoga.```

***5/17/2018 - Change Log:***
```
-updated source links for cave and roulette
-other minor fixes
-updated CBG essay
-updated CV
```

***6/27/2018 - MOTD:***
```Just yoga and blockchain noncense (see what I did there?)```

***3/28/2018 - MOTD:***
```Happily unemployed and contemplating my next adventure :). Made some great progress on what will likely be my first Haskell library on hackage. Still needs one more pass and thorough testing but otherwise I'm pretty happy with it. Doing lots of yoga too :o.```

***1/31/2018 - MOTD:***
```I'm just updating this MOTD (again) to indicate that I still somewhat care about updating my website. Feeling older and happier. I'm not really making any art right now. I've been writing a little but I doubt I'll have anything blog-worthy anytime soon. Working on my first Haskell module which may see the light of day.```

***1/31/2018 - Change Log:***
```
-minor updates to about page to indicate that I'm still alive. Empty promises.
-updated CV. Apparently I already had SEMC on my CV so I just added "C++" to it...
-added more animal pictures. Changed thumbnail to pictures of animals page.
-added Misc. art 2
-It just occurred to me these changes logs might be mistaken for commit messages. If this were the case, these logs would look something more like "atoehusaon". Only dvorak users will understand D:.
```

***5/17/2017 - MOTD:***
```I'm just updating this MOTD to indicate that I still somewhat care about updating my website.```

***5/17/2017 - Change Log:***
```
-minor updates to about page to indicate that I'm still alive.
```

***12/2/2016 - Change Log:***
```
-added top secret page for selected works pdlla.org/posts/hidden/2016-11-30_selected_works.html
-probably some other minor changes I neglect to mention
```

***10/20/2016 - MOTD:***
```Perfect Woman is finally released on Xbox ONE. This only took me 4 years and at least 4 totally traumatic events. Pretty good if you ask me. If you happen to have an XB1 with kinect, please consider giving the game a try :).```

***10/20/2016 - Change Log:***
```
-added full gallery shots to $2.01
-added blender contributor to CV
-updated bio on about page to be slightly more cynical.
-updated link to Aliah Magdalena Dark's website.
-oops, accidentally had an unfinished blog post linked. Moved it to HIDDEN.
```

***8/23/2016 - Change Log:***
```
-added markdown image resizing using http://stackoverflow.com/questions/14675913/how-to-change-image-size-markdown
-renamed "Log" to "Change Log" change logs.
-added tor workshop images to $1.81
-corrected date in Perfect Woman URL and added coming out on Xbox ONE blurb :)
-added page for 77-pieces in unkown as portfolio reference
-padded my resume a little :)
-beatdown in misc games now links to a hidden page
```

***7/27/2016 - MOTD:***
```Starting to include change notes with my updates. I like this because then I wont forget to updated the MOTD and people will know when the last time my site was updated and be able to determine with a greater degree of certainty whether my website has fallen into no-update hell or not. Deciding on the best format to do this. Currently deciding on lumping both together and storing at the bottom of this about page. Markdown code block formatting is annoying and weird but I'll just stick to what I have for now..```

***7/27/2016 - Change Log:***
```
-changed code block font color to a dark gray.
-updated $1.81 to include Tor workshop in July. No pictures because I forgot to take them and lost my notes.
-updated $1.81 to include Mushroom workshop in July.
-updated formating in $1.81 to be more ugly so content is more visually differentiable at first glance.
```

***6/30/2016 - MOTD:***
```I forgot I'm suppose update my MOTD. I should probably add an update log too.```

***12/4/2015 - MOTD:***
```I'm currenty taking a break from growing mushrooms to focus on making games and understanding online gaming communities. My next hobby/discipline will probably be taxidermy but Los Angeles is a bad place to find road kill.```
