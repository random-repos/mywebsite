---
title: Blockchain Consensus
date: December 10, 2019
tags: blog, blockchain
---

The [blockchain consenus series](https://docs.thundercore.com/consensus-protocols-101.pdf) goes in depth on foundational and historical background of consensus as an academic field and shows how it applies to blockchain. It culminates in a description of the [PaLa consensus algorithm](https://eprint.iacr.org/2018/981.pdf) which is the best consensus algorithm of its class.

You can read it here as a PDF or skim through the series on [Medium](https://medium.com/thundercore/consensus-series-preliminaries-a3bab33ae09) (the PDF is much better formatted).

In addition, I also authored:

- The [ThunderCore Whitepaper](https://docs.thundercore.com/thunder-whitepaper.pdf)
- A [cute slide deck](https://static.wixstatic.com/ugd/b45014_aa81ff3475064676ac4840d7662ff0b5.pdf) explaining ThunderCore Consensus (or Proof-of-Stake consensus in general)
- A semi-introductory [article](https://medium.com/thundercore/a-deep-dive-into-smart-contracts-ec446c27887f) on smart contracts

These articles were written by me as an employee of [ThunderCore](https://www.thundercore.com/)  While some of the content is specific to ThunderCore--and EVM compatible public blockchain--the ideas are broadly applicable. The content quality is also quite good :). Enjoy.

Finally, the entire consensus series is also available in Chinese (translated by ThunderCore). Note the formatting is off and some pictures are missing so you're better off reading the english PDF if you can

- [Preliminaries 1](https://mp.weixin.qq.com/s/sEhOJCXmKgZKawPjwKs4MA)
- [Preliminaries 2](https://mp.weixin.qq.com/s/x9MMcoW4sY8rMHB8IWyNSw)
- [Preliminaries 3](https://mp.weixin.qq.com/s/lHlBD-hE9JIdNJQt4Icn9w)
- [PBFT](https://mp.weixin.qq.com/s/_1lvrw_DTMI8Cxs7x1jMkw)
- [PoS](https://mp.weixin.qq.com/s/C5ovX2aaESQx3kjI0pRUTw)
- [Thunderella](https://mp.weixin.qq.com/s/kDP-GXSLjmIWrYIAlOWmPg)
- [PaLa](https://mp.weixin.qq.com/s/x1DiVBRpSc59YHEnlIqFmg)
