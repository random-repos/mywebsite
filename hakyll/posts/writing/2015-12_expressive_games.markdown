---
title: Expressive Games
date: December 26, 2015
tags: paper, game design, expressive gameplay
---

This is a short article on games as an expressive medium. These are games that say or try to say interesting things in interesting ways. I want to dissect the components that operate a games narrative and how these come together with all other aspects of the game to form its meaning of the work as a whole (or as my High School English teacher liked to say "MOWAW").

We enjoy games differently than how we enjoy non-interactive narrative forms. Both tell stories, but games engage the reader by challenging them as a player. This is the underlying reason why the path for games to reach widespread critical regard has been more tumultuous than film and comics. Their mode of operation is not just to narrate but also to engage the player. The sort of stories that naturally follow from this are different then the sort of narratives that we are familiar with in the critical discourse of other mediums.

Now let’s examine how challenge and narrative so often do not go hand in hand. The first issue here is ludonarrative. When the mechanics are grounded in reality (i.e. not metaphoric) we associate them with reality but then something is off because the game rules tear at the seams of the game's diegetic structure. Even if the mechanics DO seamlessly tell the story it can be too good to be true. High budget titles are especially guilty for padding their mechanics with high production responsive animations, impossibly smooth and entirely improbable. This is the uncanny valley of game mechanics.

The second issue is much more subtle and this is what Roger Ebert was addressing when he infamously proclaimed games cannot be art (http://www.rogerebert.com/rogers-journal/video-games-can-never-be-art). But first we need to understand that games are about emergent narrative. The story must be both told and controlled by the player. The game rules define a narrative space for the player to navigate. So when we try to say games are art what are we really asking? What was Ebert really thinking? Are each and every one of the infinite narrative branches art? Or perhaps there is somehow one "correct" branch and this one is art but the other ones are not. Or maybe it is up to the player to choose a branch that is art, but then is the player the artist or is the game designer the artist? 

As a whole, games are not stories, they are machines for stories, and we must use a different set of standards to critically analyze them. Consider this coarse deconstruction of a game's narrative:

1. Local narrative (HOW)
	- This is the story that gets told with every single action that you make, and where those actions take you. Each action you take in a game represents a progression through a narrative that is unique to you.
2. Global narrative (WHAT)
	- This is your entire narrative experience of the game from start to finish. This narrative may be linear or multilinear with each step in between dynamic in the actions that are required to progress the story (e.g. Halo, Doom). This narrative may be largely dynamic with the end goal driven by the player (e.g. The Sims, Minecraft). 
3. Meaning (WHY)
	- This is the purpose of the game. Why was it made? Why did I play it? Why did I like or dislike it? What do I take away from my experience? Does the game succeed at its purpose? This encompasses the entire narrative space as well as your individual experience. It considers the artist's intent, the individual player's experience, and the socio-political context the game exists in. 

To expect a singluar directed film or novel like experience from a game narrative is simply the wrong frame of reference. But this is unsurprising as large developers, no doubt mimicking Hollywood’s success, try and deliver such an experience by superimposing film production methods onto game development. No doubt such a try hard effort would be met with such harsh criticism from a film critic. 

Let’s looks at ways games uniquely deliver expressive content. Sometimes gameplay may be metaphoric. Early art games such as "passage" and "marriage" use interaction and game design to model ideas. Such metaphors may be more direct than writing to explain complex ideas but by themselves, they are explanatory, not expressive.

Sometimes gameplay may be a subliminal aesthetic choice. Here the semiotic encoding of a message by the creator is an implicit encoding of her subconscious. The message is an emotion, where the look and feel of a game is able to communicate this feeling in its own unique way through the details of its art and interactions.

Sometimes gameplay may raise an awareness of its medium and context. This is an implicit or explicit encoding of a message through the structure of the game. A message that gets told by the existence of a decision the player is given as oppose to the experience of the player making the decision. For games, I pay special attention to mention context as well as games require a lot of context specific engagement from the player. For example, if the game is played online or offline, with friends or without, over a long period of time or briefly. These contextual decisions the designer makes (which may not always be followed by the player) guide the players experience and situate the game in a political spectrum of accessibility.

The use of the term expressive here is a little misleading. Every game, by its nature of being a human form of expression, is expressive--reflecting the character of its creator. Each interaction in a game is in reference to a deep history of game design and culture. A game designer's job is to negotiate with this history to achieve her goal. What I have proposed is a holistic approach to understanding games. From this perspective, a deeper cultural understanding of games can be achieved. For the developer, a holistic approach allows them to control the depth of accumulated knowledge in game design but also break away from its confines thereby pushing the medium forward.
