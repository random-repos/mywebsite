---
title: Practicing Inclusive Language in Code to Overcome Subconscious Bias
date: June 21, 2020
tags: language, bias, code
---

[Github](https://www.cnet.com/news/microsofts-github-is-removing-coding-terms-like-master-and-slave/) and [Golang](https://www.reddit.com/r/golang/comments/gy9ylr/go_has_removed_all_uses_of_blacklistwhitelist_and/) both recently announced removal of terms like "master/slave" and "whitelist/blacklist". Ruby on Rails made [some effort](https://github.com/rails/rails/issues/33677) towards this in 2018 and Microsoft had outlined this in their [style guide](https://docs.microsoft.com/en-us/style-guide/a-z-word-list-term-collections/m/master-slave-master-subordinate) a while back. I have no doubt many with less prominent platforms have done so as well.

I'm glad big companies are stepping up (whatever their reasons). Precedence is important. When I tried to rename such usages at my startup in 2018, my efforts were largely met with indifference. Constantly reminding seniors and VPs that our feature was called "blocklist" and not "blacklist" was perhaps not the most productive use of anybody's time. I think any effort now to rename racist terminology will be met with much more support.

Even if you don't see the value in replacing racially charged terminology that "clearly" aren't being used with racist intent, you can at least agree that terms like "safelist" and "blocklist" are more clear than "blacklist" and "whitelist" which are one step removed from their intended meanings.

***

The words in our language encode layers of meanings that exist below the surface our awareness. By changing the signifier for what is intended to be signified, we also highlight its other meanings. In this moment, we can glimpse into our own subconscious biases.

I saw the potential of this when a panelist pointed out that our use of the word "but" is often unnecessary. "*The presentation was really well organized but your delivery needs improvement*". In our culture, we're taught to preface our negative feedback with a positive comment so as not to disappoint others. Yet by contrasting the feedback against a positive comment we frame it as negative and defeat our very intent. No one wants to eat your complement sandwich. She suggested in these cases the word "and" is more appropriate. "*The presentation was really well organized and your delivery needs improvement*". This seemed easy enough until I tried. It was a huge mental hurdle and an experience I can only describe as cognitive dissonance. Just thinking about this simple switch, required confronting my own negativity and fear of disappointing others. All this was encoded in a 3 letter word. This issue is dear to me. My desire to collaborate with others could not compromise with my inability to provide constructive feedback without negative judgement. Making the switch to "and" gave me a safe opportunity to practice a healthier mindset. Now it's easy for me, and I find myself judging others much less. Instead, I focus on how I can learn from them and better support their goals. Unlike challenging centuries of racial injustice or gender discrimination, I think few will judge you for switching to more a positive outlook. I encourage everyone to practice replacing "but" with "and".

Language is powerful. It reflects our cultural values and is shaped by our history. It encodes countless assumptions and enables us to communicate incredibly complex relational messages with just a few symbols. You can find a shocking demonstration of its influence in ["A Person Paper on Purity in Language"](https://www.cs.virginia.edu/~evans/cs655/readings/purity.html) by Douglas Hofstadter (TW: it's clearly satire, but the racist neologism get pretty intense).

[![](http://static1.1.sqspcdn.com/static/f/1542080/27705295/1506822236863/comicdoctor.png)](http://www.lunarbaboon.com/comics/doctor-1.html)

Finally, let's not forget the power inclusive language has on its audience. It will nudge at the rusty gears in readers who are use to seeing the more familiar terms. It can be safe and subtle. Sliding in a singular they, for example, will unlikely meet the ire of even the most intolerant listener. Last but not least, it's a means for allies to identify each other in unsafe environments.

***

Randomly, since 2017, Microsoft Office has had a [spellchecker option to highlight biased language](https://twitter.com/garius/status/1271035621461708801). This feature was never really promoted, and it's not too late!
I love that you can choose which biases you want to work on. Change is hard and must be self driven. @garius's tweet gets it right, "*Your spellchecker will now help you to start overcoming your subconscious biases*". I love what Microsoft is doing and hope to see others take inspiration.

[UPDATE: It looks like [textio](https://textio.com/) has a similar if not [more sophisticated feature](https://textio.com/blog/watch-your-gender-tone/13035166463). I haven't tried it but looks promising based on the examples. It catalogs words that aren't gendered explicitly but none the less speak to a gendered audience. For example "inspired by" is offered as a more inclusive alternative to "driven by". I wonder if heir choice of wording [in the context of a job listing] is "You could attract more women to apply by changing your language" which seemingly reinforces the idea that women aren't "driven".]

***

I knew in 2018 that asking everyone to use inclusive terminology in one minor feature at a struggling startup would amount to nothing. That wasn't the point. I wanted to challenge myself and others to confront the bias internalized through our use of language. These things take time and visible results often don't come until many years later. We're starting to see the fruits of those labors now and there's a lot more work to do. Don't be afraid and don't give up.
