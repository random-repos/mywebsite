---
title: Show Reviews 2021
date: January 18, 2021
tags: reviews
---

Quick reviews with random comments on various things I watched in 2021 😗. Shows are rated out of 5 emojis. Enjoy!

Spoiler hiding stuff not working due to hakyll pandoc fail or whatever sorry 😱

### Squid Game

- "Alice in Borderland" but with better acting and maybe less interesting plot
- Ali soooo humble :O I'm glad they included the scene where he sticks it up to his boss in E2
- I was so cringe when the old guy was monologuing on his tug of war experience and now I think he's a literal edge lord.

### She-Ra

- Love the fist pound thingy in the She-Ra transformation sequence. Love these small details, really mixing masculine and feminine movements.

### Hunter x Hunter (2011)
🃏🍆🧒🏻

- Killua and Gon seeing Hisoka's actual hardon while Bisqy is thirsting was so hilarious it took me a whole minute to notice it was also really messed up 😱
- I was most disappointed in the power levels of many characters feeling inconsistent over the course of the show
- Killua's character development is really good. Can't say so much for the rest of the characters.
- Ikalgo + Palm best characters :O

### Owl house
🏚🍞🍞🍞🦉

### Amphibia (Season 2)
🐸🙎🏼‍♀️🙎🏻‍♀️🙎🏽‍♀️🐸

- I never thought about it so much but this whole time I was like there's no way Marcy is Asian. An Asian main character AND and Asian supporting character 🤯. Comedic that I could barely fathom this. Truly unprecedented.
- It's not explicit but I think the distance between Sasha and the other two cuts across racial lines. Sasha's brazen and selfish disregard is something I distinctly remember as trait my white friends had (albeit I had no non-white friends at the time). To me, these characters are not just "character who happens to be XYZ" (perhaps an occasional callback (e.g. Anne in a Pha Biang)) but characters who are XYZ and their relations (in this case) are fundamentally shaped by the fact they are XYZ.
- >! Super Sayanne :O! I can't wait for Sasha and Marcy to get their powers.
- Frobo!
- >! I was so worried Poly was gonna stop being cute after she grew legs, but she's literally the same except with tiny little legs
- >! Look on Reddit/YT for scenes where Anne (and the other two) display their latent powers.
- The last episode was GREAT! I wish they didn't rush it so quickly. That was a lot of plot and character development in one episode.
- >! Love the opening for S3 at the end of S2 so cute. I guess Disney had to let kiddos like me know that Marcy was gonna be OK.
- Obvious plot line comparison between Amphibia, ToH and SVTFOV. Yet each show is so unique in their own right too.
- Why does Anne never use Tritonio's ancestral sword when it shows up in the OP?? :(
- SVTFOV is still my fav of the 3 which is a bit of a bummer because it had the least progressive characters. Ponyhead is a rather problematic character too 😱. I'm just a sucker for the non-sense character shenanigans in that show.

### The Twelve Kingdoms
🐀🐀🐀🐀🙆🏼‍♂️

- I watched this based on AnimeFeminist's top anime recs and was not disappointed
- The ranka childbirth thing was such a missed opportunity for non-heteronormative narratives :(
- Why do hanjyaku have human forms 😑. That really cheapens whatever story is meant to be told through their oppression...
- The power and politics at play in the third arc (where Youko uncovers the corruption in her kingdom) was so well written. Youko's character development is really well integrated into this part. The first proclamation speech she gives to her ministers at the end of the arc (E40) was EPIC! Feminist flex.
- How did Suzu live for 100 years and stay so immature 😅. Also is it really that hard just to learn the language?
- Shoukei's character development was great and much more plausible
- I definitely felt a little sorry for Asano who gets unwillingly caught up in this nonsense, goes a little crazy, and dies after failing at a doubly pointless task that no one asked him to do (or maybe Suzu let him do it out of pity). Then I remembered that this happens to female supporting characters all the time and I think it's pretty funny for the useless male romantic supporting character to get the same treatment. Also feminist flex.

### Attack on Titan (Season 4)
🤯🤯🤯🤯🙀

### Shokugeki no Soma (Season 5)
🍖👙🍖

- Episode 1 was painful. Obligatory beach episode. But to what end!? If this is what you really want go read some hentai doujinshi.
- Stop telling Nakiri to smile
- I've never been more cringe-triggered in an anime that hearing Saiba repeatedly call Nakiri "Nakiri-hime"
- I can't handle these tsundere characters 🤮.
- They introduced all these sweet Noir characters and montaged through most their battles 😡 why!?
- (all seasons) ignoring the rather cringey sexist character design. the characters are still likable, the writing is good, and really innovates on the shonen/cooking genre.


### The Owl House (Season 1)
🏚🍞🍞🍞🦉

- Why is everyone so mean to hooty 😭
- OMG (you know what scene I'm talking about) https://www.youtube.com/watch?v=yhNB0vO7FxI I just love how the lead changes back and forth. I must have rewatched this 10 times

### Queen's Gambit (Episodes 1-4)
🐴🏰

- Creepy male gaze opening shot 😱
- Creepy older men glancing over at definitely-only-13-year-old Beth as she walks through the hotel Lobby in Cincinnati
- Creepy hotel scene photoshoot ends in plot twist. Love seeing representation of older gay men creepy who are creepy and inappropriate to underage women.
- Some of the later more explicit shots of Beth are surprisingly "no filter". Still creepy.
- And by creepy I mean in the sex offender sort-of way

### Bojack Horseman
😱😂🍆🤯👍🏽

#### Season 5
- Flip was such a sleaze at the end of S1 and I had a hard time imagining what could come of that. Filbert turned out to be a bigger part of S5 than I would have imagined and the season was the strongest one yet and the most nuanced take on feminist issues in the show yet that it barely registers and feminism. Of course the feminist takes in previous seasons had developed points beyond the great punchlines, but S5 took it even further. Having said that, Bojack the feminist was a little too cringe to me, or perhaps it hit too close to home 😱.
- >! The Filbert/Bojack alter ego device came to full play in the final episode. We knew Bojack/Filbert was his own villain from the start and the film noir but the rug is pulled out from under us. We're relieved that Bojack doesn't do anything horrible to Gina only for Filbert to strangle Sadie later on set when we thought the story was over. The setup? Bad Mr. Peanutbutter trying to convincingly strangle Filbert's wife an episode earlier.
- >! The deepest/darkest moment of the show is Gina asking Bojack to lie about what happened, so that Gina's fame is not first and foremost that of a survivor. Where we realize how true her words are (in fiction and in reality) and worry that Bojack is gonna fuck it up in the interview and make it all about him again. But he doesn't and we cheer because we know that's how it had to be. Moments like these reveal more of what our society is (and why it's awful) than any outspoken feminist dialog because we already know it's true.
- >! I love the duality between Diane and Bojack. Both fucked up from childhood. But Bojack is rarely held accountable for his actions and continuously drains the emotional energy of the women (and men) around him. Success follows Bojack around (comedically so in the case Secretariat) and he doesn't know what to make of it. Diane has fewer excuses and develops faster than Bojack (and is all around less fucked up). The fight between Diane and Bojack in the last episode really spells out the similarities and differences. It also draws light on how patriarchy harms men in the sad reality that is Bojack. Diane in particular points out that no one (in particular, no girl croosh expose) can hold Bojack accountable for the harm he's caused, only Bojack can do that.
- Todd's sexbot storyline was the best backdrop the season could have asked for 😂. The feminist undertone of the main storyline was subtle enough that it needed a foil to highlight its own brilliance.

#### Season 6
- I loved the change in the opening sequence :O
- >! It took me a while to understand Diane's decision to distance herself from Bojack at the end and perhaps the most important consequence must accept for his actions. It's implied that his life as a rich celebrity will continue as before with the unicorn show, but his relationships will not.
- I love that the animals doing animal things are NOT a stand-in for race, and this is made with the occasional race related moments in the show with Diane and Todd. Todd's moment is season 6 is appropriately farcical. I do read the animals as a stand in for diversity though--each bringing their own uniqueness to the world.

### The Good Place (Season 1-4)
🥔🥔🥒

- The best part of this show is that they realize what kind of person Elon Musk is between S1 (2016) and S2 (2017) and you can tell from the jokes they make.
- Jason Mendoza being casually objectified as a dumb hottie with no qualification is a huge step forward for representation of Asian American men on the screen 🌶️🌶️🌶️. That really wasn't hard.
- "Race, Sex, and Nerds: From Black Geeks to Asian American Hipsters" by Ron Eglash is a must read to better understand characters like Jason and Chidi.
- The Good Place is a pretty colorblind show. The diverse cast overcomes conflicts with no real racial or gendered tension.
- I like that in S3 and S4 they make a few racism/sexism aware jokes. Like when Brent tosses his used towel at Chidi who is nothing but exasperated. I wonder if jokes like these help enforce more equitable social norms or if they mask the hard fought victories that make these jokes possible and distract from all the work that still needs to be done. At the very least they are hilarious.
- >!I doubt Brent was based off of Brett Kavanaugh and I don't like to be mean but the scene where he lists off all his best buds with bro-y nicknames (just like Brett does in his hearings) was pretty funny 🔥🔥🔥.
- >!Brent is inconspicuously missing in last episode of the show when every other human makes a cameo 😂.
- >!I was flowing tears when Jason decided he needed to go through the door. Not sure why 🤷‍♀️
- >!It took me half the episode to realize Hypatia (Patty) is played by Lisa Kudrow better known as Phoebe from friends. A cute callback to an earlier episode where Michael appeals to the Judge by listing characters from Friends who don't deserve to go to the bad place and then whittling the list down to just Phoebe. For some reason, the fact it took me half an episode to realize why Patty looked so familiar makes me feel even smarter for catching this one.
