---
title: Inclusive Naming Guidelines
date: June 8, 2021
tags: language, code
---

# Inclusive Naming Guidelines

This article contains guidelines to make your feature or variable naming more inclusive. Feel free to share, copy, edit, etc.

Be mindful of your choice of words and use them to support an inclusive work environment. Avoid using language that is:

- Generalizing or stereotyping of specific groups of people
- Needlessly gendered
- Violent or militaristic
- Sexually charged
- Denigrating, devaluing
- Expressing anger

Below are lists of words to try to avoid due to their racist, prejudiced, or otherwise harmfully exclusive history and associations. If you see these, please consider updating them and referencing this page for justification.

In many cases, the terms here connote or denote meaning that serves no purpose in the context it is being used. Therefore these terms are simply not efficient in communicating it's intended meaning.

## Never Use
| term      | reason                                                                                                                                                                                                                                                                                 | alternatives                                                           |
|-----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------|
| whitelist | see blacklist                                                                                                                                                                                                                                                                          | allowlist                                                              |
| blacklist | Relies on the (racist) connotation that black = bad to communicate its intended technical meaning of a list of disallowed entities.                                                                                                                                                    | denylist, blocklist                                                    |
| master    | It is important to acknowledge the centuries of enslavement of African Americans when using the term. I'm doubtful that using this term as a feature or variable name can also adequately convey the necessary cultural deference so as not to erase such serious part of our history. | controller, manager, director, conductor, orchestrater/or, owner, main |
| slave     | see master                                                                                                                                                                                                                                                                             | worker, agent, soldier, unit, pawn                                     |

## Avoid

| term                                       | reason                                                                                                                                                                                                                                                                                 | alternatives                                                                 |
|--------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| hang                                       | avoid when usage might have militaristic or racist meaning                                                                                                                                                                                                                             | frozen                                                                       |
| destroy, kill, dead                        | violent language                                                                                                                                                                                                                                                                       | stop, stopped, halt, end, shutdown                                           |
| blind                                      |                                                                                                                                                                                                                                                                                        | unperceptive, isolated, independent, disconnected, lacking visibility/vision |
| poor                                       |                                                                                                                                                                                                                                                                                        | weak, bad                                                                    |
| chairman, manpower, man in the middle, etc | male-gendered language reinforces male primacy                                                                                                                                                                                                                                         | chairperson, power, middleperson                                             |

For longer and more official looking resources to share with your colleagues, please see [Inclusive Naming project's word replacement list](https://inclusivenaming.org/language/word-list/) or [Microsoft's "Bias-Free Communication" blog post](https://docs.microsoft.com/en-us/style-guide/bias-free-communication).

[^1]: Strictly speaking, "blacklist" denotes a list of disallowed entities. However, as the first recorded use of blacklist occurs at the time of mass enslavement and forced deportation of Africans to work in European-held colonies in the Americas, it reasonable to think that its denotative meaning was derived from the racist association of black and bad (and "blacklist" is not the only example of this). Even if we are to believe this is not the case, the connotative reliance of the racist association between black and bad in the term "blacklist" can not be avoided in present times anymore. After all, did you have to ask what "blacklist" meant the first time you saw the term?
