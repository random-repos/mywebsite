---
title: Meeting Moderation Guide
date: September 9, 2022
tags: DE&I
---

## Community Guidelines for critical discussions

Go over these before each of your meetings! 

- Speak for your own own experience 
  - Use I statements, talk about yourself and your own experiences. Don't speak for other's experiences. 
- Lean into discomfort and engage with new ideas
  - Today is an opportunity for you to practice at this and grow your perspectives
- Both/And thinking
  - some ideas may feel contradictory to what you already know. 
  - Two things can coexist that seem to be at opposite ends. 
  - How can these contradictory views expand upon your own perspectives?
- Take care of yourself 
- Don't assume trust. We acknowledge this may not be a safe space for everyone.
  - If you came here just to listen and you are dialing in virtually, you’re welcome to turn your camera off.
- Confidentiality
  - The lessons can leave. Keep specific identifying information in the room.
- Nonclosure
  - Don't expect to find all the answers today, instead think of this as another step along the way in an ongoing discourse towards building a better, stronger and more inclusive community.

In addition, for work-related discussions in professional settings, it may be useful to add the following guideline:

- Leave your job (title) at home
  - Outside of this discussion, we should hold each other accountable for being our best selves and doing impactful work.
  - Within this discussion, we want to be equal individuals regardless of what our roles and responsibilities in this company are.
  - We may express hopes and ideas that differ from what folks are actually doing in their jobs. Let's make this a source for exploration not a source for conflict

These guidelines were inspired by those used by [Outlet](https://www.acs-teens.org/what-we-do/outlet/) is their training programs!