---
title: Meeting Moderation Guide
date: September 9, 2022
tags: DE&I
---

Simple guidelines for moderating discussion spaces in professional enviornments

# Responsibilities

- Establishing a braver space so people feel comfortable expressing their thoughts
- Moderate if needed to ensure conversation stays productive and all voices are being heard
- Ensure meeting stays on topic and on schedule

# Moderator Job Outline

## Invitation

Invite everyone into the space and set the tone for a respectful, fun and productive meeting!

### Example

Hi everyone, before we start this meeting let's all take a moment to arrive and settle into this virtual space. <pause> Thank yourself for being here and doing all this extra work for what you believe in :))).


## Introductions

If appropriate (fewer than 15 people, first meeting or lots of new folks), go around the room and ask for introductions. Pick a question that seems relevant for the topic of the meeting and always give folks the option to pass for any reason.

### Example

Let's go around the room and introduce ourselves. Please share your name, and only if you're comfortable, your preferred pronouns and what inspired you to attend today's meeting.

## Guidelines

Below are ways to set guidelines for the discussion.

Choose 3 guidelines to help establish a braver space. Searching the internet for "safer space guidelines" or "braver space guidelines" produces many examples. 

For bonus points, choose an intention based on the meeting topic and tailorize your 3 guidelines to support it.

### Example

#### Topic: Female representation in Roblox 
#### Intention: Bringing awareness to gender dynamics in meetings

- Be mindful of space, challenge yourself to take up less space if you know you like to share a lot, and take up more space if you know you're more reserved.- - Before engaging ask yourself if what you are about to say is supportive, productive, and working towards our goals. Emphasize listening and asking questions to learn.
- Practice inclusive language: avoid saying "guys" since we're not all guys! Use this space today as an opportunity to practice.

Finally invite everyone to consent to space by sending a +1 into chat if they all agree.


## Moderate

In most cases, you'll let the meeting run itself and step in only to switch from one topic to the next when the time is right. 

You are not responsible for creating space for others to discuss and you are NOT responsible for their words. 

Sometimes you can offer kind reminders to keep everyone true to the intention of the space:

    If you notice someone who seems to have something they'd like to speak up but has not, invite them to share.
    If you notice the conversation going of topic, kindly interrupt and bring things back in focus.
    If you notice one person taking up too much space or making unproductive remarks, kindly interrupt and bring things back in focus.

## Professional Topics and Heated Conversations

At times, we will discuss topics that relate directly to our professional work. For example, we may be discussing how feature A systemically marginalizes group B when someone who worked on feature A is the room. Discussions must actively avoid putting people on the spot and holding them responsible for their work. 

It is normal for folks to get aggressive about a topic they are passionate about or defensive if they feel their work is being criticized. As moderator, you must do your best to focus everyone's attention on the constructive conversations. The goal is ultimately to learn from each other and support each other under a shared goal of promoting diversity and inclusion. If appropriate, emphasize this aspect explicitly in the guidelines for the week.

#### Tips for handling these situations:

- identify the pattern early and interject before conversation starts becoming unproductive
- emphasize that discussions should not be personal in the guidelines for the week
- invite quite participants to share their opinions which may shift the conversation in a different direction
- ask questions in order to uncover underlying reasons for the heated conversation
- directly acknowledge address the nature of the conversations

## Wrap up and Encourage Followup

Take a few minutes to conclude the meeting at the end. Thank everyone for showing up and perhaps share a few insights you have gained to foster a culture of learning (smile). Remind folks in the meeting to followup on any action items by connecting with relevant product owners and domain experts.



