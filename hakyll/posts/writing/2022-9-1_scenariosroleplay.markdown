---
title: Scenario Roleplaying Discussion Workshop
date: September 9, 2022
tags: DE&I
---

# What Is the Scenarios Workshop

## Intention

- develop tools for identifying scenarios where our sense safety personal values are being violated or challenged
- practice how to and understanding when to
  - call in/out
  - engage/disengage
  - advocate
- practice graciously receiving positive or negative feedback given under varying circumstances
- practice taking responsibilities for our mistakes while maintaining self-compassion and connection with others
- practice embodying, modeling, and fostering inclusiveness in the workplace
- Walk out of these sessions feeling empowered to
  - challenge real workplace situations that go against our values of diversity, equity, and inclusion
  - grow in our interactions at work

## What do we actually do?

- come up with "scenarios" (brief workplace/life situations that we feel challenged by)
- discuss and unpack these scenarios to understand them better
  - how did these behaviors impact us
- identify ways we might engage in these scenarios for a more desirable outcome
- role play these scenarios to practice our skills

## How Do We Do It

### Agenda

- Introduce the meeting and go over Community Agreements
- roleplay
  - choose a scenario
  - solicit volunteers to roleplay different characters in the scenario
  - mix it up and improvise
  - have fun!
- discuss
  - share first impressions
  - share our own experiences
  - discuss strategies for how to engage with the scenario
- iterate
  - roleplay again applying strategies from previous steps
- repeat


## Coming up with Scenarios

We solicit scenarios from our participants! If we're out of scenarios, we'll spend the first 30 minutes in break-out rooms to come up with scenarios during the meeting. 

## Scenario Guidelines

- workplace based scenarios are encouraged and this is not space for talking shit about people/incidents
- IF you must base the incident off something that happened to you,
  - no identifying information
  - give some time between incident
  - re-skin the scenario
- topics to avoid (not because they aren't important to discuss but we aren't equipped to handle them)
  - politics
  - explicit sexual harassment
  - physical violence

## Scenario Template

#### context:
  - brief sentence explaining the context and settings of your scenario
  - avoid judgements in your context! Leave that for discussion!
#### dialog: 
  - use your favorite fake names
  - provide line-for-line dialog


## Example Scenarios

---------

### An App By Any Other Name

#### Context: 
Jake is an EM and manager his close-knit team of engineers Bob, Pierre, Chris and Ramón. Today Jake is holding a cross functional team meeting with their product manager Alice and their designer Pat.

#### Dialog:
Jake: Welcome team, we are meting today to review Alice and Pat's designs on our upcoming product.
Chris: Hey guys, super appreciate the work y'all did! We implemented Pat's designs and I felt it really augmented our sexy nurse app!
Alice: I'm going to assume by sexy nurse app, you mean the personalized medical assistant app we are working on and discussing today.
Pierre: Bro, we've been calling it sexy nurse app for weeks. You haven't come up with a product name yet.
Jake: Alright guys, lets stay on track, we're not here to discuss the app name today
Stupid Language

---------

### Language

#### Context: 
Alice and Bill are having a discussion at their desk, Claire is overhearing at a neighboring desk

#### Dialog:
Bill: This project is a disaster
Alice: Ugh, the whole thing was done in JavaScript which is STUDID FUCK. We should rewrite it in C++.

---------

### I Just Said That

#### Context: 
Jake is the manager of the devops team. He's holding a meeting with the team members Bob, Pierre, Alice and Pat to discuss ideas for addressing recent trend in service failures

#### Dialog:
Jake: Okay, so the service broke again this weekend and we really have to fix things up.  Give me your ideas.
Pat: Oh gosh, again, what did I miss? What happened this time. UGH.
Bob: How about we break it up into microservices
Alice: What if we modulate the tachyon beam?
Jake: Cmon focus folks, this is a serious issue
Alice: Sorry (haha)
Pierre: We've been running this service for years, why are we running into issues now?
Pat: We've seen a gradual increase in users since we started, it's natural for us to hit scaling issues
Pierre: What about breaking up into microservices?
Jake: Hey Pierre, I think you might be onto something there, can you elaborate more?
Be Targeted



-------

Made in collaboration with Raymond Keller