---
title: San Mateo Restaurant Review
date: May 18, 2018
tags: blog, food
---

Since moving here February 2017, I've gotten to sample many of the great food places in San Mateo. Here are reviews of my favorite places. I'm heavily biased towards family owned places that offer humble home style recipes and friendly service.

### Shanghai Noodle

[Shanghai Noodle](https://www.yelp.com/biz/noodle-shanghai-san-mateo) offers

Personal Favorites:
- Pig Ears: Holy cow (pig?) these are good! They cut them very thin at an angle and cook the flesh super tender while the cartilage remains crunchy. It's seasoned with just coarse salt and cilantro. These are by far the best pig ears I've had. I suspect the secret is baking soda.
- Xiao Long Bao: A staple in Shanghainese cuisine! SHN has a solid take. I've eaten here so much it's become my favorite XLB.
- Radish Box: Grated daikon radish deep mixed with some magic and deep fried in an odd but appealing oval box shape. There's some magic spice or herb in there that makes the flavor sublime. These are pretty small so pretty low stakes if you're not sure you'll like this unique Chinese dish.
- Salt Meat Rice: Rice, salted pork, and A-cai cooked in a clay pot. Aromatic and delicious. If I had to make a meal here out of just one dish, I'd pick this one every time.
- A-Cai: stir fried with garlic. Mind blown. This place does the best fried greens. You can ask for other veggies off the menu and A-Cai is the best (it use to not be on the menu but they have since added it).
- Braised Pork Noodles: At least that's what it says on the menu. It's actually slices of pork with lots of fat cooked in rice wine lees. SHN offers several noodle dishes that are the same bowl of noodles (with bok choy and a hard boiled egg) plus a side. You can always ask for rice or the side by itself if you aren't interested in the noodles. The noodles they come with are great too, it's just that the pork is so much better.

- wild rice shoots - (茭白)

Unique:
- All the noodle dishes are pretty unique here as you might expect. I'm apparently really picky Chinese noodle dishes so I have a hard time discerning good and bad. Having said that, the Scallion Shredded Meat Noodles really stand out. I also normally don't like fried noodles (crispy noodles with clear sauce and ingredients poured over) and I enjoyed theirs so I guess that's saying something.

Avoid:
- Don't get the dumplings or spring rolls, they were kinda awful TBH.

### Central Market and Cafe

https://www.yelp.com/biz/central-market-and-cafe-san-mateo?hrid=EINZe7vqe8FqTcEond31rQ&rh_type=phrase&rh_ident=bahn_mi

I'm sad I discovered this place so late after moving here. They are only open for lunch and I usually only passed by in the evenings. This unsuspecting place is a small cafe in a kinda dingy convenience store. They have a menu consisting of generic sandwiches on a Heinekin branded blackboard.

Eventually I discovered they actually have menus and a regular lunch time crowd. I've since worked my way through most their menu.

The place is run by two incredibly friendly Vietnamese Sisters. You'll also see their parents every now and then.

They make me feel bad if I don't go for lunch after yoga so I go there all the time. Good thing the food is amazing.

something about roasted garlic in everything

Personal Favorites:

- Tofu Banh-Mi: The best I've ever had! It's just fried medium firm tofu with the typical affair of pickled veggies and it was REALLY good. P.S. the secret ingredient is Mayo. Don't get the pork Banh-Mi, it was stringy and overcooked. I haven't tried the chicken yet.

- Shrimp or Spring Roll Bun:

- Pho: Their pho is phenomenal. I asked about the broth and it's made from chicken and lots of veggies. The flavor of the roasted garlic..

### Ravioli House

You can't go wrong with any combination but I listed my favorites below.

Somehow the sandwiches come out to more than the sum of their parts. My only complaint is that the portions are too small. Of course, your other choice is the ravioli which probably 3x the calories in carb heaven.

Personal Favorites:

- Traditional Meat Ravioli with Pesto :
- Vodka Sauce with Pancetta: They don't always have this but this sauce is incredible.
- Veggie sandwich: One of the best I've ever had. It's your standard affair of avocado, sprouts and bell peppers in place of the meat. They spread a modest amount of pesto and that somehow brings everything together.
- Turkey sandwich: my new favorite. Again, a typical turkey sandwich with cranberry sauce that goes above and beyond the sum of its parts.


## Honorable Mentions

### Draeger's (sandwiches from their deli)

Introduced to me as "more expensive than Whole Foods", Draegers is a family run grocery that is, indeed, more expensive than Whole Foods. I'm probably only listing this because my dear friend has been shopping there everyday for years and knows the family well. They run a rocking and well priced sandwich bar using their awesome and considerably more expensive deli. Enjoy their friendly service and shop while they make you a kicking sandwich on sliced bread. The greens are really fresh and their pickles are stellar. The egg salad and meat loaf are my gotos. Also try their house made kobe pastrami. Ok, honestly, those are the only 3 I ever tried.
