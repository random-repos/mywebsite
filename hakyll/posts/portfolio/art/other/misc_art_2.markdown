---
title: Misc. Art 2
date: January 31, 2018
projectdate: ongoing
tags: art
thumbnail: thumbnails/miscart2018.png
---

### Ceramics Centering Guide (Nov, 2019)
Commissioned stickers by [Adeline Ducker](http://cargocollective.com/adelineducker)

<img src="/images/misc_art_2018/ceramics_centering.png" alt="drawing" style="width:700px;"/>

You can download the [original PSD](/images/misc_art_2018/ceramics_centering.psd) as well. Please use in your beginning ceramics class!!!!!!

### Bitcoin STICKERS (Nov, 2018)
Commissioned stickers by [Adeline Ducker](http://cargocollective.com/adelineducker)

Download and use the [whole set + bonus stickers (not shown below) + uncolored version + extra bonus potato stickers](/images/misc_art_2018/crypto_stickers.zip).

![](/images/misc_art_2018/s1.png)
![](/images/misc_art_2018/s2.png)
![](/images/misc_art_2018/s3.png)
![](/images/misc_art_2018/s4.png)
![](/images/misc_art_2018/s5.png)
![](/images/misc_art_2018/s6.png)
![](/images/misc_art_2018/s7.png)
![](/images/misc_art_2018/s8.png)

### Bytecraft Ethereum DAPP (with Anton Bobkov) (June, 2018)
bytecraft is an [open source](https://github.com/antonbobkov/city_building_game_v1) Ethereum smart contract with a JS/Haskell web front-end. You can't play it anymore because we took down the server. Sad.

<img src="/images/misc_art_2018/bytecraft.png" alt="drawing" style="width:512px;"/>
