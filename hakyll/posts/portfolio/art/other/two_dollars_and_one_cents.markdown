---
title: $2.01 / "The Master's Tools will Never Dismantle the Master's House"
date: May 15, 2016
projectdate: May 12th, 2016
tags: art
thumbnail: thumbnails/201.png
gallery: auto ../../../../images/201
---

Instead of making art for my Design | Media Arts MFA thesis, I started an art service. We will come up with a concept for you and take it to execution. We're current closed to new business.

```
Have (your) art for a low price $2.01.

We analyze your work and style and produce work for your own to save yourself time and money!! Let our experts take care of concept and implementation at great quality. We work very fast. After contract is made, we immediately go to the drawing board. After materials attained, turn around time is within 24 hours!!

We are experts area of new media arts including bio art, food art, feminism, post-internet art, post-capitalism, game art, youtube, algorithm, VR, AR, environment, and much much more.

We also promote your work at no extra cost. Your work will auto be included in a show at UCLA May 12th with no selection process!

Attached is document explaining process. Ask questions. Please direct all inquiries to peter lu <chippermonky@gmail.com>.

Best wishes,
-peter
```


![](/images/201/inline/greatart.png)

-------------------------

When our business first opened, as a promotional offer, we put our cient's work in an exhibition in the New Wight Gallery on May 12th, 2016. The exhibition was curated by one of our many talented clients, [Echo Theohar](http://cargocollective.com/SkyeTheohar), because we believe curation is also a form of art. This exhibition contained works from 15 artists.

### Curator's Statement

```
I started off looking for new media art exhibits to playfully activate the exhibition space generously provided for by the UCLA School of Arts and Architecture. When assembling this show, I noticed all works negotiated themselves against a shared anxiety of our precarious relationship with our technology driven future.

The layout of the space draws connections between the numerous voices echoing this sentiment. As we struggle with/against the forces that define a collective identity for ourselves, so too is this struggle recreated at a perceivable scale within the confines of these walls.

The stage is now set for the climax of the anthropocene. Like two particles accelerating towards each other in a trans-national multi-billion dollar underground proton synchrotron, approaching 99.999999% the speed of light, the elements of our existential reality are also about to crash spectacularly. Upon collision, new secrets of the fabric of our existence, never before seen, will become apparent (or not!).

Until then, the works in this exhibition will dance and cry together in joy and sadness.

-Echo Theohar
```

#### [Gallery Catalog](/images/201/catalog.pdf)

![](/images/201/inline/show.jpg)
![](/images/201/inline/echo2.jpg)
![](/images/201/inline/echo3.jpg)

-------------------------

## OUR CLIENTS

- [Anton Bobkov](http://www.math.ucla.edu/~bobkov/)
- [Breelyn Burns](https://www.facebook.com/treelyn.ferns)
- [Symrin Chawla](http://www.symru.net/)
- [Aliah Magdalena Darke](https://prettydarke.wordpress.com/)
- [Adam Ferriss](https://www.adamferriss.com/)
- [Jesse Flemming](http://jessefleming.com/JRF2015/home.html)
- [Sanglim Han](http://sanglimhan.work/)
- [Kate Hollenbach](http://www.katehollenbach.com/)
- [Peter Lu](http://pdlla.org)
- [Chandler McWilliams](http://chandlermcwilliams.com/)
- [Neil Mendoza](http://www.neilmendoza.com/)
- [mia0](http://hsinyulin.info/)
- ????????
- [Jennifer Steinkamp](http://jsteinkamp.com/)
- [Sharon Traweek](http://www.genderstudies.ucla.edu/faculty/sharon-traweek)
- [Theo Triantafyllidis](http://theotrian.tumblr.com/)

## WORKS FROM OUR SATISFIED CUSTOMERS

<div id="gallery"></div>
