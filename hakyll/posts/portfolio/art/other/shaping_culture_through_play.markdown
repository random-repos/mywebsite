---
title: Shaping Culture through Play
date: June 21, 2020
thumbnail: thumbnails/shapingculturethroughplay.png
---

This page tracks an ongoing line of my research tentatively titled "Shaping Culture through Play"

Listed in chronological order:

- [World-Centric Massively Multiplayer Online Games](http://pdlla.org/posts/writing/2015-3_CBG.html)
- [World-Centric Blockchain Games as Playground for Radical Markets](https://blog.radicalxchange.org/blog/posts/world-centric-blockchain-games-as-playgrounds-for-radical-markets/) [(github)](https://github.com/pdlla/WCBG)
- Shaping Culture through Play: How the Magic Circle can help us Build Better Democracies - RadicalxChange 2020 talk]
  - [slides](https://docs.google.com/presentation/d/1drHLp95zBYkyrGsMW5QK5sIlpDpFsT7LkObzE3HrwpM/edit?usp=sharing)
  - [recording](https://www.twitch.tv/videos/655378963?t=36h18m7s)
