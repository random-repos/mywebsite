---
title: Misc. Art 1
date: October 28, 2015
projectdate: 2015
tags: art
thumbnail: thumbnails/miscart2015.jpg
gallery: 
---

### Laptop Cooking (Dec, 2015)
This is a laptop I tried to swindle from the [now deceased startup](https://www.crunchbase.com/organization/77-pieces) I use to work for because they owed me 250 ish dollars. A few years later, the video card semi-broke (possibly because I dropped it). Apple [recalled](https://www.apple.com/support/macbookpro-videoissues/) their 2011 MPB. I brought it in to their store (a dystopian multi-ethnic technological fantasy land) once and they said they would make a one time exception to fix it at no cost under the recall because my water censor triggered. 3 months later, it broken again and they wouldn't fix it so I [baked it at 350F for 7 minutes](http://ales.io/2014/03/09/how-to-bake-a-mac.html) and presto. 

![](/images/misc_art_2015/laptop1.jpg)
![](/images/misc_art_2015/laptop2.jpg)
![](/images/misc_art_2015/laptop3.jpg)

UPDATE (March, 2016): parents just informed my the lappy died again :(.


### One ceramic cup filled with not-very-useful wooden kitchen utensils (Oct. 2015)
To be placed in the gallery's cupholder. For 6UWY425 Car Gallery.

![](/images/misc_art_2015/thumb.jpg)


### Biased Data, A Panel Discussion on Intersectionality and Internet Ethics (Nov. 2015)
Presented by the technodiversity team at the voidLAB. A video recording of the entire panel can be viewed [here](https://www.youtube.com/watch?v=pciMwEAlH7Y&list=PLeHWLvSsSQpib4ov4x0Fda40vSOXA836j).

![](/images/misc_art_2015/BiasedDataBanner.jpg)


### You can have your cake and eat it too: the technocrat's dream, now reality (Mar. 2015)
A cake with a picture of itself projected on it. Cake, Projection, exotic woods and expensive knives.

![](/images/misc_art_2015/cake.jpg)


### Me Eating a Chicken Carcass (Dec 2014)

<iframe width="420" height="315" src="https://www.youtube.com/embed/GpM2Ea6oBf4" frameborder="0" allowfullscreen></iframe>