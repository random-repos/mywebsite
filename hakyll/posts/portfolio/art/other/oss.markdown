---
title: Open Source Software
date: February 21, 2020
projectdate: ongoing
tags: code
thumbnail: thumbnails/oss.png
gallery:
---

A list of open source projects I work on or have worked on in some way. You can really just visit my [github](https://github.com/pdlla) page TBH. This is just a partial list as I'm too lazy to move some of my repos onto github at this moment.

# not-games

## stuff that you might actually want to use
- [tinytools](https://github.com/minimapletinytools/tinytools-vty)

## stuff that you would not actually want to use
- [haskell-ffi-cabal-foreign-library-examples](https://github.com/pdlla/haskell-ffi-cabal-foreign-library-examples) see very long title.
- [linear-tests](https://github.com/pdlla/linear-tests) QuickCheck `Arbitrary` instances and property tests for the [linear](https://hackage.haskell.org/package/linear) package.
- [smarties](https://github.com/pdlla/smarties) behavior tree EDSL in haskell.
- [potatotrader](https://github.com/pdlla/potatotrader) library created for the purpose of bot trading shit coins.
- [animalclub](https://github.com/pdlla/animalclub) WIP library for breeding goats and other things.

## guides
- [react-lua-tutorial](https://github.com/minimapletinytools/react-lua-tutorial)

## contributor
- [Blender](www.blender.org) My list of contributions can be seen [here](https://developer.blender.org/differential/query/biusJjrFLIIO/). Yeah ok, it's just some UVs. If you ever texture map Suzanne you can thank me 🐒.

# games
(most of these have their own page on my website, this is just a partial list too)

- [cave](https://github.com/pdlla/cave)
- [roulette](https://github.com/pdlla/cave)
- [burn n turn](https://github.com/antonbobkov/burn_n_turn)
- [Perfect Woman](https://github.com/minimapletinytools/perfectwomangame)
