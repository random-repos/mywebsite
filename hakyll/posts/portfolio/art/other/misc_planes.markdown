---
title: Misc Planes
date: January 1, 2025
projectdate: ongoing
tags: art, food
thumbnail: thumbnails/misc_planes.JPG
gallery:  auto ../../../../images/misc_planes
---

Pictures of planes I've made.

<div id="gallery"></div>