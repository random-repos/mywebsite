---
title: Central Market
date: January 1, 2025
projectdate: ongoing
tags: art, food
thumbnail: thumbnails/central_market.png
gallery:  auto ../../../../images/central_market
---

Pictures of food from Central Market in San Mateo, CA.

<div id="gallery"></div>