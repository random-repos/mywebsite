---
title: Animals
date: July 19, 2015
projectdate: ongoing
tags: animals
thumbnail: thumbnails/picturesofanimals.jpg
gallery:  auto ../../../../images/animals
---
Pictures of animals I've had the fortune of forming some sort of relationship with. Unfortunately, I'm missing a lot of photos.

<div id="gallery"></div>