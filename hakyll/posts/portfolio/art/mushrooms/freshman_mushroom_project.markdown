---
title: Freshman Mushroom Project
date: October 15, 2015
projectdate: 2015
tags: mushrooms art
thumbnail: thumbnails/freshmanmushroomproject.png
gallery: freshman_mushroom_project/_DSC5881.jpg freshman_mushroom_project/_DSC5928.jpg freshman_mushroom_project/_DSC5933.jpg freshman_mushroom_project/_DSC5941.jpg freshman_mushroom_project/_DSC5954.jpg freshman_mushroom_project/_DSC5891.jpg freshman_mushroom_project/_DSC5929.jpg freshman_mushroom_project/_DSC5937.jpg freshman_mushroom_project/_DSC5944.jpg freshman_mushroom_project/thumb.png freshman_mushroom_project/_DSC5896.jpg freshman_mushroom_project/_DSC5930.jpg  freshman_mushroom_project/_DSC5938.jpg  freshman_mushroom_project/_DSC5947.jpg freshman_mushroom_project/_DSC5901.jpg freshman_mushroom_project/_DSC5931.jpg freshman_mushroom_project/_DSC5939.jpg freshman_mushroom_project/_DSC5948.jpg freshman_mushroom_project/warning.png
---

Performances by [Goddess LeeAnn](https://twitter.com/goddessleeann), [Michael Q. Schmidt](https://en.wikipedia.org/wiki/Michael_Q._Schmidt) and myself. Video documentation shot by [Justine Sto. Tomas](https://twitter.com/justine_sto).

## VIDEO
<iframe width="960" height="540" src="https://www.youtube.com/embed/U-K4qVESxvE" frameborder="0" allowfullscreen></iframe>

## WORDS
- The parts are all cut by hand from maple. A straightforward assembly because of the quarter inch threaded inserts. 
- The body is assembled from four parts, tightly held together, giving the structure a sturdy core.
- The front legs elegantly curve out in two dimensions. Fixed to the body by 3 bolts each. 
- The back legs separate from the body as they splay to the sides more than the front. Though they look stronger, they are held onto the body less efficiently than the front. Nonetheless, they provide more than enough support.
- The top is a simple plank upholstered in ostrich level which was all that was immediately available. The color and texture are nice, but it may stain with bodily fluids if used.
- The pads are clamped on and sturdy. The legs of thus require no modification.

## DESCRIPTION
- Both performers were given the freedom to improvise as they felt fit. Written directions below.
- Lee Ann sits on the throne. She looks like a doll but has the aura of a goddess. She is indifferent to Michael and to the audience. She looks straight forward with a goddess like authority. She may look indifferently at the audience but knows she commands their desires. She may read a book or check her phone. If she does, she does with a doll like grace that she owns and she is indifferent to the fact that people look upon her. 
- Michael is nude on the floor. The horse is on the floor dissambled. Michael enthusiastically assembles the horse that he built himself. He is infatuated with his own accomplishment but believes he is performing his task objectively. Periodically, he glances towards Lee Ann but pretends like he is indifferent to her. The penis pump is also on the floor. Michael completes the assembly of the horse. When the horse is assembled, he walks into Lee Ann's space without looking at her and takes the two ceramic vases on the pedestals. He brings them back to the horse and ties them onto the hole in the back of the horse and inserts the flog. He straddles the horse acting like he is testing it out. He glances towards Lee Ann, expecting her to come over and give him a good spanking, however soon he becomes self conscious and supresses himself from acting overly desperate. After a minute of testing the horse out, he dismounts. He is embarassed and ashamed but tries to hide it. He slowly disassembles the horse with much less enthusiasm, neatly stacking the parts as if they are going right back to the storage. I walk into the space and gently tap Michael on the shoulder. Then I walk to the curtains and let the audience know the performance is over.

## PHOTOGRAPHS