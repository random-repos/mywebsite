---
title: Throne Room
date: April 8, 2015
projectdate: 2015
tags: mushrooms art
thumbnail: thumbnails/mushroomthrone.jpg
gallery: mushroom_throne/IMG_1708.jpg mushroom_throne/IMG_4856.JPG mushroom_throne/SHRUNKIMG_7065.jpg mushroom_throne/IMG_1726.jpg mushroom_throne/IMG_4869.JPG mushroom_throne/SHRUNKIMG_7089.jpg mushroom_throne/IMG_4825.JPG mushroom_throne/SHRUNKIMG_7007.jpg	mushroom_throne/IMG_4827.JPG mushroom_throne/SHRUNKIMG_7039.jpg mushroom_throne/IMG_4853.JPG mushroom_throne/SHRUNKIMG_7044.jpg mushroom_throne/danger.png
---

Performances by [Mistress Iris](http://www.dominatrixiris.com/) and myself.

- You enter the room through a small entrance. The atmosphere is starkly different, as if to suggest entering into a different world.
- Your attention is directed forward to the powerful woman on the throne staring you down.
- There's an emasculated man in a robe staring up at the woman with dirty feet and vaseline smeared over his face. 
- What is the relationship of love and power between the man and woman?
- Perhaps your vision distorts because everything is so foreign you don't know how to center yourself.
- Is this a hallucination? No it's not.
- Maybe you look at the ceiling, maybe you talk out loud and take pictures as a means of keeping yourself grounded.
- The room is filled with odd objects covered in mycelial growth.
- A gnarly rope walls you off from entering the scene. It is not for you to be a part of.


