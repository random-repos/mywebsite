---
title: Misc. Mushrooms '14
date: January 3, 2015
projectdate: 2014
tags: mushrooms art
thumbnail: thumbnails/miscmushrooms2014.jpg
gallery: misc_mushroom_2014/IMG_1356.JPG misc_mushroom_2014/IMG_1754.JPG	misc_mushroom_2014/IMG_2936.jpg	misc_mushroom_2014/IMG_3053.jpg misc_mushroom_2014/IMG_1369.JPG	misc_mushroom_2014/IMG_2949.jpg	misc_mushroom_2014/IMG_3531.jpg misc_mushroom_2014/IMG_1747.JPG	misc_mushroom_2014/IMG_1770.jpg	misc_mushroom_2014/IMG_3023.jpg
---
Various mushroom related work from 2014:
