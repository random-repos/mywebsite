---
title: $1.81
date: November 12, 2015
projectdate: ongoing
tags: mushrooms art
thumbnail: thumbnails/181.gif
gallery: 
---

Various goods and services I sell for $1.81. Below is some sparse information about some of the things I've done. 

---------------------------

## $1.81 Tor/Deep Web introduction (Jul. 6 2016)

```
On Thu, Jun 30, 2016 at 12:14 PM, Peter Lu <chippermonky@gmail.com> wrote:
Hi everyone, for those of you who are still around for summer.

I'm giving an introduction to Tor/Deep Web Wednesday July 6 @ noon. I'll talk about what it is, how it works, and how people break it. Cost is $1.81. Meet in grad studio. Please RSVP. 

Topics:
-encryption introduction
-Internet infrastructure
-TOR protocol introduction
-exit nodes
-hidden services
-TOR attacks

Following that, I'm doing a final playtest for Perfect Woman before we publish! If you want to help out, please come to the Game Lab @ 1pm!

If things go well, maybe I will give similar introductions to neural nets and bit coin.

Best,
-peter
```

[![scale500](/images/dollar_eighty_one/torworkshop1.jpg)](/images/dollar_eighty_one/torworkshop1.jpg) [![scale500](/images/dollar_eighty_one/torworkshop2.jpg)](/images/dollar_eighty_one/torworkshop2.jpg)

## $1.81 Mushroom growing workshop (Jul. 2 2016)
A workshop on growing mushrooms. Workshop slides are viewable [here](https://docs.google.com/presentation/d/1A0nrzZCvWCW-Q8ACsmBhgEOxG1waL7nIFQr9Sl2YDq4/edit?usp=sharing).

![](/images/dollar_eighty_one/mushroomworkshop.jpg)


```
On Thu, Jun 30, 2016 at 12:14 PM, Peter Lu <chippermonky@gmail.com> wrote:
Hi friends, I'm hosting a mushroom growing workshop this Saturday July 2 @ 11am. Only 4 spots left RSVP!!! Location is my apartment on the westside ########################

We'll cover the following NINE topics:

1. presentation of mycology and mushroom growing basics
2. DIY equipment and technique overview
3. (hands on) preparation and sterilization procedures for WBS spawn
4. (hands on) spore syringe preparation
5. (hands on) spore mass inoculation method
6. (hands on) grain 2 grain inoculation method
7. sawdust and straw bulk substrate preparation demo
8. bulk substrate inoculation demo
9. fruiting methods demo

Cost is $1.81. That's about 20c a topic!! 

Also some mushroom art will be for sale for $1.81 limited supplies!!!

Sincerely,
-peter
```

## $1.81 Dinner + Cooking Show Screening (Mar. 2016)

![](/images/dollar_eighty_one/dinner1.jpg)

![](/images/dollar_eighty_one/dinner2.jpg)

I showed [EO1 Daikon Cake](https://www.youtube.com/watch?v=xM9XNbZz0UM) of my [Mom's new youtube cooking show](/posts/portfolio/art/other/pitaloveshummus.html) :O.

## $1.81 Ceramics Sale (Dec. 2015)

![](/images/dollar_eighty_one/ceramics1.jpg)

![](/images/dollar_eighty_one/poster2.png)

![](/images/dollar_eighty_one/ceramics2.jpg)

![](/images/dollar_eighty_one/changecup.jpg) 

*A small porcelain cup filled with change. I imagine one person decided to put their change in the cup and several other people followed. I was very adament about giving everyone exact change and not taking donations telling people "this is not a charity".*
