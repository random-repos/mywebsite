---
title: Clay e-mails
date: April 8, 2015
projectdate: 2015-present (ongoing)
tags: art ceramics
thumbnail: thumbnails/clayemails.jpg
gallery: auto ../../../../images/email_ceramics
---

### 2022
In 2022, I downgraded from e-mails to hand written letters. As before, etched into wet clay, bisque, glazed, and fired again. Eventually to be sent to to their recipient.


For Jennifer, a would be lover, all too soon, who finally awoke me from my dream as it was turning to a nightmare

![](/images/clayemails_jennifer.png)


### 2015 

E-mails scratched into wet clay. Bisqued, glazed and fired again. Photos are first taken and the ceramic object is then given to its intended recipient but only if I still feel like it.

<div id="gallery"></div>
