---
title: Misc. Ceramics '10-'13
date: April 8, 2015
projectdate: 2010-2013
tags: art ceramics
thumbnail: thumbnails/miscceramics.jpg
gallery: misc_ceramics/IMG_6953.JPG	misc_ceramics/IMG_6986.JPG	misc_ceramics/_MG_8277.JPG	misc_ceramics/_MG_8292.JPG	misc_ceramics/_MG_8297.JPG misc_ceramics/IMG_6970.JPG misc_ceramics/IMG_6996.JPG	misc_ceramics/_MG_8278.JPG	misc_ceramics/_MG_8296.JPG misc_ceramics/_MG_8300.JPG
---
Some ceramic pieces made between 2010-2013:
