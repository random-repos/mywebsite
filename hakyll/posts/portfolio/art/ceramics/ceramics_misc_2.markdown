---
title: Misc. Ceramics 2
date: Nov 18, 2019
projectdate: 2018-present (ongoing)
tags: art ceramics
thumbnail: thumbnails/misc_ceramics_2.jpg
gallery:  auto ../../../../images/misc_ceramics_2/gallery
---
Ceramics projects from 2018 to present

### Pickups (May, 2018)

Pickup lines and my phone number scratched into glazed ceramic cups

![](/images/misc_ceramics_2/pickups.jpg)

Pickups is a rather sad portmanteau of "pickup" and "cups".

### Random Stuff 2018-present
  
<div id="gallery"></div>
