---
title: Perfect Woman
date: September 22, 2014
projectdate: 2012-2016
thumbnail: thumbnails/perfectwoman.png
tags: games
---


<!--<object data="http://www.perfectwomangame.com" width="100%" height="5000px"> <embed src="http://www.perfectwomangame.com" width="100%" height="5000px"> </embed></object>-->

Perfect Woman is coming out on Xbox One September 14th 2016! For more information please visit [our website](http://www.perfectwomangame.com).

<iframe src="//player.vimeo.com/video/75890888?portrait=0&amp;color=fe5607" width="500" height="281" frameborder="0" align="center" webkitallowfullscreen  mozallowfullscreen allowfullscreen></iframe>

Perfect Woman is a game inspired by the ubiquitous personality questionnaires featured in women’s magazines and the female roles they define. There are so many such roles emphasizing aspects of family, career, experience, sex and more. But these can not possibly characterize the depth and complexity of a woman’s life. Perfect Woman uses these stereotypes as building blocks for you to be your OWN perfect woman.

You build your own Perfect Woman story by choosing and performing a diverse cast of fun and interesting characters across seven different stages of your life. While certain life choices may be more or less difficult than others, it's up to you to decide what's best for yourself. The game is structured into progressive levels, each one representing a different stage in a woman’s life. Starting as a fetus in your mother's womb, players must perform in front of a Kinect to mimic various poses that appear on screen as a means of acting as that character.

Perfect Woman is a game about celebrating diversity and choice rather than condemning conformity. We hope to extend this philosophy outside of the game as well as we see games as a medium to promote acceptance, personal growth, community and love.

<iframe src="https://player.vimeo.com/video/133094093" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

Perfect Woman is played using motion controls. Match the poses on the screen with your body and the Kinect will track your movements. You start as a fetus in your mother's womb which also serves as a tutorial and introduction for the rest of the game. Once you are born, you can make your first life decision and decide who you want to be at age 9. At each stage, you can choose between 4 different characters. Match the pose below the choice you want to make your decision. While performing may be difficult, we believe making the right choices should be simple and intuitive.

The core mechanic involves performing characters by matching poses to the best of your ability. At the end of ecah stage, you are presented with cutscene based on your performance as a reward for your hard (or not so hard) work! The game will then tell you how your current choice and performance will effect the difficulties of the choices you can make in the future.

There are no right or wrong choices though, and there is no right or wrong way to play the game. Your choices may differ, and if you perform poorly, you may even die young. But this is just another fact in your Perfect Woman life story.

![](/images/perfect_woman/thumb.png)

Lea and Peter first met and started collaborating on Perfect Woman at the UCLA Game Lab in 2012. With a mutual interest in examining social issues through interactivity, and finding new and interesting ways to tell narratives in games, they started collaborating on Perfect Woman. The game is currently complete and has been received with critical acclaim including an IGF nomination. The two developers are in the process of publishing the game on a Xbox One to make the game available to a wider audience.

Lea Schoenfelder is an artist and animator from Germany. She has made several critically acclaimed games addressing social issues in her iconic style. Her games include the IGF award winning [Ulitisa Dmitrova](https://www.youtube.com/watch?v=6kikDI7E6EY) exploring youth poverity in St. Petersburg and the gorgeously illustrated and poignant [Ute](http://www.rockpapershotgun.com/2014/01/17/s-exe-ute-by-lea-schonfelder-nsfw/).

[Some more writing on Perfect Woman](posts/writing/2016-13-3_perfect_woman.html)

[Lea's thesis paper on Perfect Woman](/images/perfect_woman/PerfectWomanBooklet.pdf)

Open source code for Perfect Woman is available [here](https://github.com/pdlla/perfectwomangame) and [here](https://github.com/pdlla/perfectwomanfiles).
