---
title: Roulette
date: September 22, 2014
projectdate: 2008
thumbnail: thumbnails/roulette.png
tags: games
---
Play Six-Barelled Russian Roulette against Brandon.  

![](/images/roulette/roulette.png)
<br />
[windows](http://users.dma.ucla.edu/~chippermonky/roulette/roulette_fixed.zip), [mac](http://users.dma.ucla.edu/~chippermonky/roulette/roulette_mac.zip), [source](https://github.com/pdlla/roulette)  
[video|lose](http://vimeo.com/16226166) [video|win](http://vimeo.com/16226199)

Created by Peter Lu,  
with help from Simon Wiscombe,  
and starring Jake Parker

Also see Roulette's cute [original website](http://users.dma.ucla.edu/~chippermonky/roulette/)