---
title: Beamtimes and Lifetimes ♠♥♦♣ The Card Game 
date: June 6, 2016
projectdate: 2016
tags: games
thumbnail: thumbnails/btlt.png
gallery: 
---

![](/images/thumbnails/btlt.png)

```The unique breed of particle physicists constitutes a community of sophisticated mythmakers--explicators of the nature of matter who forever alter our views of space and time. But who are these people? What is their world really like?```

Beamtimes and Lifetimes ♠♥♦♣ The Card Game (BTLT) is a card game based on Sharon Traweek’s book of the same name. In BTLT, you play as a research group and compete with other groups over physicists, grants, hardware, and beamtime to solve the mysteries of our making.

*BTLT was commissioned by Sharon Traweek for [$2.01](/posts/portfolio/art/other/two_dollars_and_one_cents.html) and freely available.*


### REQUIREMENTS:
- 2-4 players
- 1x standard deck of cards
- 2-4x unique sets of at least 25 tokens--one set for each player. 

### SETUP:
- distribute one set of tokens to each player.
- split a 52 card deck into suits:
	- ♥ hearts - physicists
	- ♠ spades - detectors
	- ♦ diamonds - unknown particles
	- ♣ clubs - grants


### ♦ DIAMONDS:
- shuffle the diamonds and lay 6 cards face down in a circle oriented radially. These are particles that probably exist and together, they form your research facilities particle accelerator (synchrotron)
- lay the 5 of remaining cards above the accelerator face down in a row. These are particles that probably don't exist ;).
- put the remaining 2 cards away face down, these particles are unknown and do not exist.

### ♣ CLUBS:
take cards 1-6 and place them face up in a row. These are the available grants.

### ♠♥ SPADES + HEARTS:
shuffle these together and reveal five in a row below the accelerator. These are detectors/physicists that are on the market. Place the remaining cards face down at the end of the row. Below this row are distinguished physicists but the game starts with no distinguished physicists.

### PLAYER TOKENS:
represent each player's influence and capital. These tokens are also used to indicate credit such as which group is working on which problem.

### ON YOUR TURN:
you may take 1 of the following actions:
1. Apply for grants. Declare which face up grant (♣) you are apply for. Roll 1 dice, if it is greater than or equal to the value of the grant you are applying for you successfully obtain the grant. Gain tokens equal to the value of the grant. Take the grant card and put it face down. When there is only one face up grant left, reveal all grants.
2. Hire physicists or purchase a detector from the market. The card furthest from the market deck costs 1 token. The second furthest cost 2, and so on. When you take a card, shift all cards down (so they are less expensive) and reveal one card from the market deck that now costs 5. You may instead hire a distinguished physicist for 5 tokens if there are any.
3. Assign a physicist (♥) to a problem. This action costs 1 token. Place your physicist face up perpendicular to an open problem. If it is empty, the physicist is now the group leader working on this problem and their lab is located at this section of the accelerator. If it is not empty, place it below the physicist that is already there. Your physicist is now a collaborator. Put your token on the  physicists to indicate that they belong to your research group.
4. Build a detector (♠) at the section of the accelerator where you have a physicist who is the leader. At this point, you and all collaborators may look at the unknown problem (♦). See RESOLUTION section below to continue.

- additionally ONCE at the END each turn, you may pay tokens to look at an unknown particle (♦) card that is not on the accelerator to verify that this particle probably does not exist. After looking at the card, place 1 token on top of the card. The cost to look at the card is equal to the number of tokens on top of the card + 1.
- if you have 5 or more cards in your hand, you must take an action according to the table below:
	- if you can build a detector, you must build a detector.
	- if you can't build a detector but can assign a physicist to a problem, you must do so.
	- otherwise, you must apply for a grant or skip your turn.

### RESOLUTION:
When a detector is built, all players collaborating on the problems can look at the card. When we say a problem is in range of a physicist and a detector, we mean that the number on the particle card (♦) in between the numbers on the physicist card (♥) and detector card (♠) or equal to one of the two numbers. Aces are considered to be 1 and Jacks, Queens and Kings are considered to be 11, 12 and 13 respectively.
1. If the problem is NOT in the range of the detector and one of the physicists, all physicists working on the problem lose their jobs and are shuffled back into the marketplace deck. The detector goes back to the player who built it. Take the particle card representing the problem and one of the particle cards from the 5 probably non-existing particles with the fewest number of verifications, shuffle them together, and replace randomly. If there is more than one choice of probably non-existing particles, the player who built the detector can decide which  to take. Remove all verification tokens from the two new (or possibly old) particle cards.
2. If the problem IS in range, the lead physicist may decide publish the result. If so, reveal the card to all players and distribute tokens according to the table below. The lead physicists now goes to the distinguished physicists pile and may be hired by any player for 5 tokens during their turn. All collaborators go back to their player's hand. The leader may also choose not to publish the results. If so, the all physicist go back to their player's hand. In both cases, shuffle the detector back into the marketplace. Finally, take this problem card, and all adjacent particle cards that are not being worked on, shuffle them together and place back face down randomly.
3. If the problem exactly matches a physicist and the detector (i.e. they are all the same number), then the problem is SOLVED. Reveal the card to all players and move the token on the leader onto the problem. All other physicists go back to their player's hands. Distribute tokens according to the table below. 

### TOKEN DISTRIBUTION:: 
X = different between detector and physicist (if it is a collaboration, choose the physicists such that the problem is in range of the detector and the physicist and X is smallest)

- if it is NOT a collaboration, take 10-X tokens.
- if it is a collaboration, the leader takes 7-x tokens and each collaborator takes 5-x tokens.
- you can not take negative tokens (just take no tokens)

e.g. The leader is a KING and the collaborator is a 9. The detector is an 8 and the particle is 8. In this case, X = 1, the difference between the detector (8) and the nearest physicist such that the particle is in range of it and the detector (9). Since it is a collaboration, the owner of the collaborator (9) received 5-1 = 4 tokens and the owner of the leader receives 7-1 = 6 tokens.
e.g. The leader is a JACK and there are no collaborators. The detector is a JACK and the particle is a JACK. In this case, x = 0 and the problem is solved. The owner of the leader (JACK) receives 10-0 = 10 tokens.
e.g. The leader is a JACK and the collaborator is 2. The detector is a 5 and the particle is a 4. In this case, X = 3, the difference between the detector (5)  and the nearest physicist such that the particle is in range of it and the detector (2). Since it is a collaboration, the owner of the collaborator (2) received 5-3 = 2 tokens and the owner of the leader receives 7-3 = 4 tokens.

### WINNING:
The game ends when all problems have been solved. The research group(s) with the most solved problems is declared the winner. 




### FAQ:

What happens to the physicists whose value matches a particle that surely does not exist?
   Some physicists are just not destined to be great. These physicists may either work hard to narrow in on a solution or move on and become biophysicists instead.


