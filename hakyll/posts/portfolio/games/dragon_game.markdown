---
title: Burn & Turn
date: September 22, 2014
projectdate: 2010?
thumbnail: thumbnails/dragongame.png
tags: games, pluanbo, robotbear
---
<iframe src="http://player.vimeo.com/video/32022226?title=0&amp;byline=0&amp;portrait=0" width="420" height="300" frameborder="0" align="center" webkitAllowFullScreen allowFullScreen></iframe>
<br />
An original arcade action game. A mish-mash of present and past ideas and aesthetics.
Created in collaboration with Anton Bobkov (pluanbo).

*Kill! Capture! Loot! Burn & Turn is an arcade styled action game. Capture the princesses, defend your tower, collect and stack powerups for devastating combos.*

For more information please visit [RobotBear](http://www.robotbear.org).
