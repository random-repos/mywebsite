---
title: MOBA hero ideas
date: November 5, 2022
projectdate: ongoing
tags: MOBA
thumbnail: thumbnails/moba.png
gallery:
---

Below are various MOBA hero ideas. Free for anyone to use.

Upgrade abilities are enabled based on respective MOBA mechanics (e.g. DOTA Aghanim's Scepter/Shard)

### Helicopter Person

Helicopter Person is a large strong bodied female mechanic. Her personality is dominating and masculine but in a playful way. Her hobbies include fixing things, breaking things, and making adults cry. She's also very reckless. Helicopter Person is also called GREASE because saying Helicopter Person everytime is too much work.

Unique Mechanic: OVERHEAT

GREASE has a unique stacking buff heat. Heat stacks are lost at a rate proportional to the number of heat stacks in accordance to [Newton's Law of Cooling](https://en.wikipedia.org/wiki/Newton%27s_law_of_cooling).
If GREASE builds up too many heat stacks she enters a recovery mode for several seconds where she can no longer use her abilities. After leveling her ult, GREASE will enter OVERHEAT mode before going into recovery mode.

Q - Rev (Vroom Vroom)

GREASE Rev's her engine dropping a cloud of smog and gains an engine stack. Engine stacks give a small burst of movement speed. Rev __has no cooldown__ but builds heat very rapidly. Smog clouds slow enemies. Each engine stack grants GREASE an empowered basic attack. Attacking an enemy consumes all heat stacks

Upgrade: In OVERHEAT, GREASE concentrates the explosion in her next auto attack target, cutting both player's HP in half and pushing both players back.

W - Blow

GREASE actives her pneumatic fan dealing damage and pushing back all enemies in a cone. The gust is so strong that GREASE also gets pushed in the opposite direction dealing damage to anyone that she hits. Fan reduces heat and will also push smog clouds.

Upgrade: In OVERHEAT, GREASE unlocks a new ability. When used, GREASE removes her engine and lobs it at a target location. Her engine explodes on impact dealing damage and pushing all enemies back.

E - Spark

GREASE releases the electric potential energy built up in her engine damaging and briefly stunning everything in a radius. All smog clouds that are in this radius are lit, destroying them and dealing damage to anything in the smog cloud. All  smog clouds overlapping with lit smog clouds are also lit.

R - KABOOM (KABOOM)

GREASE's ultimate unlocks a new passive. When overheating, GREASE enters OVERHEAT mode for 1.5 seconds. In OVERHEAT mode, GREASE is silenced, does not lose rev stacks and unlocks a new active ability.

GREASE gains a new targeted ability when in overheat mode. When activated, GREASE directs her engine explosion at the groud sending herself up into the air in the target direction. GREASE and a bunch of engine debris lands at the target location after 2 seconds slowing everything in a radius. If this ability is not activated before the overheat mode ends, GREASE will still use the ability targetting her current location. The explosion damages and stuns everything in a radius including GREASE!

Upgrade: in recovery mode, GREASE gains a new skillshot ability. GREASE throws a piece of engine debris at the target location dealing 1 damage and applying a 0.1 second stun. Each activation of this ability extends the recovery period.

![](/images/misc_art_2018/grease_concept_adeline_ducker.png)

Commissioned sketches by [Adeline Ducker](http://cargocollective.com/adelineducker)



-----


### C.O.W

C.O.W is a half-woman half-cow wrestler and body builder towering at 7' When not fighting, C.O.W is often seen at the gym wearing an aged "FEMINIST AF" shirt.

Unique Mechanic: Stardom

Upon activating and connecting with each of her basic ability, C.O.W builds hype with her audience. If this is done with all 3 abilities in quick succession C.O.W enters SUPER mode for 5 seconds. In SUPER mode COW has increased movement speed, takes reduced damage, and deals additional damage. After activating each of her abilities, fans can be heard cheering at a volume proportional to their hype.

Q - Strength

C.O.W. does the archer pose yelling "STRENGTH" and gains "strength" for 1 second gaining faster move speed. If C.O.W attacks an enemy with "strength", she does a drop kick sending the target hurling backwards and sending a shock wave in the direction of attack that deals damage.

W - Courage

C.O.W does a Rear Lat Spread pose yelling "COURAGE" and gains "courage" for 1 second gaining improved defense. that replaces Courage with Body Slam. Body Slam is a targeted activated ability that hurls C.O.W at target location dealing damage and slowing all units in a radius.

E - Resilience

C.O.W does Most Muscular pose yelling "RESILIENCE" and gains "resilience" for 0.2 seconds. If C.O.W is hit by an attack or ability with this buff, all damage and effects are nullified and the buff is replaced with "nevertheless she persisted" for 1 second. Attacking a unit with "nevertheless she persisted" deals the additional damage and effects that was absorbed by "resilience".

R - Suplex

Suplex is a short range targeted ability that grabs a target unit and sends them directly behind C.O.W dealing damage and stunning them.

Upgrade: If C.O.W is in SUPER mode and turns around twice, suplex becomes victory. When activated, C.O.W poses by sending both hands up into the air in victory yelling "VICTORY", and gains "victory" for 1.2 seconds becoming faster, invulnerable and silenced. C.O.W's next auto attack becomes pile driver. She grabs the target and jumps, sending both C.O.W and the target into the sky for 1 second and then landing in the same location dealing damage to the target and stunning all units in a radius. While in the sky, a second short range targeted ability is enabled that allows the landing location to be changed.
