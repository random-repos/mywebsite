---
title: Kubisch
date: September 22, 2014
projectdate: 2010?
thumbnail: thumbnails/kubisch.png
tags: games
---
<iframe src="//player.vimeo.com/video/29509008" width="500" height="283" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen align="center"></iframe>
<br />
Kubisch is an innovative 3 dimensional twist on tic-tac-toe. Do not be deceived by its slick and simple controls. You'll find this game to be much more challenging than its 9 piece predecessor. Play against the computer or your peers. 

Tilt to look around, tap to select and swipe to rotate and end your turn. Connect 3 in a row to win. 

Now availabe for [Android](https://market.android.com/details?id=cube3.robotbear.org), ~~iPhone and iPad~~!