---
title: Road Story
date: December 9, 2015
projectdate: Fall 2015
tags: games
thumbnail: thumbnails/roadstory.gif
gallery: auto ../../../images/road_story
---

Road Story is an experimental game where you travel and experience America by car. Stitch together your own story from thrift store sales and found objects. 

<iframe src="https://player.vimeo.com/video/153346784" width="500" height="283" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<iframe src="https://player.vimeo.com/video/148378832" width="500" height="283" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

Made in collaboration with [Nick Crockett](http://cargocollective.com/nkrkt/) for [World Building](http://classes.dma.ucla.edu/Fall15/171/?p=8170).

##SCREENSHOTS

<div id="gallery"></div>
