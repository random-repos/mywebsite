---
title: Cave
date: September 22, 2014
projectdate: 2009
tags: games
thumbnail: thumbnails/cave.png
gallery: cave/1.jpg cave/2.jpg cave/3.jpg cave/4.jpg
---

![](/images/cave/thumb.png)
<br />
Cave is a short platform game that tells a gendered coming of age story.

[mac](http://users.dma.ucla.edu/~chippermonky/cave/cave_mac_fullscreen.zip),[windows](http://users.dma.ucla.edu/~chippermonky/cave/cave_pc_fullscreen.zip),[source](https://github.com/pdlla/cave)

[video: short ending](http://vimeo.com/16148146)
[video: long ending](http://vimeo.com/16149461)

Also see cave's cute [original website](http://users.dma.ucla.edu/~chippermonky/cave/)