---
title: EPOCH 5':' Paradigm Shfit
date: December 18, 2015
projectdate: 2015, WIP
tags: games singularity
thumbnail: thumbnails/epoch5.png
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/jOOk6y_chSI" frameborder="0" allowfullscreen></iframe>

### EPOCH 5: Paradigm Shift

It is the year 2020. you are a self-made wealthy intellectual in the still expanding tech industry. Global change is accelerating at an unprecedented rate. Kurzweil is the messiah of the new age. Do you believe in his technological utopia or do you fear it is the path to apocolypse? Do you embrace or reject with love or terror or are you ignorant and choose to live your life without such ideological complications. The planet deteriorates--despite all efforts to progress society, oppression and ecological terror persist. What will you do? What do you want for yourself? What do you want for the world? What do you believe?

EPOCH 5 is an interactive fiction game inspired by Ray Kurzweil and other proponents and critics of the technological singularity.

You can be and believe what you want and choose to engage with the world accordingly but you must engage with the accelerating growth of technology and the idea of the technological singularity.

As the CEO of a large successful private enterprise with some of the brightest minds in Silicon Valley, you have the power to shape and guide the world according to your vision. Support or prevent research and policy that will shape the present and direct the future. But time is running short and your own mortality, or perhaps humanity is at stake. You have power both over the world and your body.

These are the elements of your life: Business, Health, Research, Love.

Decide what you want for the world and observe the intricate relations between life, the world, the universe and everything.
