---
title: UCLA DMA 157 Game Design
date: December 11, 2015
tags: games USC teaching
gallery: auto ../../images/ucla_dma_157
---

Undergraduate upper division class offered in Design | Media Arts and taught by professor [Eddo Stern](http://eddostern.com/) and TA'd by myself. The class centers on learning basics of game design through developing board games. The class has a strong focus on creative concepts especiall in the final assignment where students are asked to design a "polemical" game expressing a personal point of view on a topic of their choice.

### [Fall 2014](http://classes.dma.ucla.edu/Fall14/157/)

### [Winter 2015](http://classes.dma.ucla.edu/Winter15/157/?page_id=5568)

### [Winter 2016](http://classes.dma.ucla.edu/Winter16/157/)

## Sample Syllabus

```
Course Description: This course provides a foundation in game design.  By focusing on non-digital prototyping, the coursework develops the concepts that are unique to game design in a manner that is isolated from any platform specific technical techniques. Throughout the course students will develop three playable non-digital games, each project will explore various aspects of game design, including: rule design, game balance, multiplayer dynamics, complexity, randomness, polemics, narrative, physical interaction, and the aesthetic and pragmatic aspects of physical game design.

The course will cover game design concepts through reading and written assignments, class presentations, discussions, playtesting sessions, and game design and production projects.

Grading: Project 1 (20%), Project 2 (%20), Project 3 (25%) 5x Reading notes (20%), 3x Game analysis feedback forms (10%) Class participation (5%)

All reading notes, feedback forms and assignment PDFs must be uploaded to the class website before the beginning of class time on the day they are due unless a midnight deadline is indicated (as 24:00). 

More than two absences without the professor’s prior permission will lower the participant’s final grade by one unit (e.g. an A will become an B). With each additional unexcused absence, the grade will drop an additional unit. Late projects will drop one letter grade, and will not be accepted if more than one week late

Required Materials: Class assignments will require the use of a wide range of mixed media such as paper, cardboard, foam core, magnets, wood, plastic, fabric, and any other materials you may want to incorporate into your projects.

Required Books:

Salen, Katie and Eric Zimmerman, “Rules of Play: Game Design Fundamentals”, MIT Press, 2004 (ISBN: 9780262240451) (Available Online)

Recommended further reading on game design:

Caillois, Roger, “Man Play & Games”, University of Illinois Press, 2001 (ISBN: 9780252070334)
Huizinga, Johan, “Homo Ludens: A study of the play element in culture”, Beacon 1971 (ISBN: 9780807046814)
Rouse, Richard, “Game Design: Theory and Practice”, Wordware Publishing, 2001 (ISBN: 9781556229121)
Crawford, Chris, “Chris Crawford on Game Design”, New Riders Games, 2003 ( ISBN: 9780131460997)
```

## Selected Projects

[Hormonal-28](http://classes.dma.ucla.edu/Fall14/157/?p=6218) by [Casey Bradford](http://cargocollective.com/ceb/Casey-Bradford)
[Will I Get Measles at Disney Land?](http://classes.dma.ucla.edu/Winter15/157/?p=7465) by [Bowan Hesslegrave](http://cargocollective.com/bowanhesslegrave/Bowan-Hesslegrave)
[Ring by Spring](http://classes.dma.ucla.edu/Winter16/157/?p=7759) by [Lizzie Zweng](http://cargocollective.com/lizziezweng)
[Ascension](http://classes.dma.ucla.edu/Winter16/157/?p=7344) by [Qiqi Zuo](http://classes.dma.ucla.edu/Fall15/161/projects/qiqi/)
[Trend Soup](http://classes.dma.ucla.edu/Winter16/157/?p=7671) by [Vita Newstetter](http://cargocollective.com/vitan)
[Flow](http://classes.dma.ucla.edu/Winter16/157/?p=7892) by [Kain Suwannaphin](http://classes.dma.ucla.edu/~tsuwannaphin/)


## Images

<div id="gallery"></div>