---
title: UCLA DMA 199 Directed Research, Board Game Finish and Production
date: April 9, 2016
tags: games UCLA teaching 
---

## -- class in progress --

A study group focused on finalizing designs for board games and putting them into production. We created the study group to allow students to continue their final projects from [DMA 157 Game Design](http://pdlla.org/posts/teaching/UCLA_DMA157.html). 

## Syllabus

```
The goal for this directed research is to take the student's game from DESMA 157 and make it ready for production (it could also be a new game). This entails refining the gameplay, polishing the design, and going through the process of getting the game professionally manufactured. The course is also intended to further develop game design and critical thinking around games. The coures is structured as a student group that meets once a week to set deadlines, discuss ideas and playtest games. We're currently scheduled to meet every Wednesday 12-2pm and Professor Stern will check in with us once every other week.

Students taking this Independent study will be expected to participate in various relevant activitise outside of our usual meeting times as they come up. This includes game lab events, outside lectures, workshops on fabrication and production resources, and additional play test days outside of our normal meeting days that will be organized when additional time is needed to playtest games before a prototype is due.

Expected work comes to an average of 12 hours/week. 3-4 hours from meetings and dicussions and 9-10 hours of work designing, fabricating, testing and producing games.

Schedule:
wk 1-3: play board games and discuss
wk 4: second prototype playtest
wk 5: play board games and discuss 
wk 6: third prototype playtest
wk 7-8: discuss fabrication techniques
wk 9: fourth/final prototype playtest
wk 10: discuss production methods
wk 11: 5x final copies due. Game Lab open playtest!
```

## Members

- [Vita Newstetter](http://cargocollective.com/vitan) : [Trend Soup](http://cargocollective.com/vitan/board-games)
- Qiqi Zuo
- [peter lu](http://pdlla.org) : [BTLT](http://pdlla.org/posts/portfolio/games/btlt_tcg.html)
- Lander