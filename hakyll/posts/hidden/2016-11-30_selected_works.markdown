---
title: 2016 Selected Works
date: November 30, 2016
tags: art
---

Below are selected works from my website. Though please feel free to browse around yourself via the navigation links above or through the gallery on my [homepage](http://pdlla.org).

### Games

- [Perfect Woman](http://pdlla.org/posts/portfolio/games/perfect_woman.html)
- [Cave](http://pdlla.org/posts/portfolio/games/cave.html)
- [Epoch 5: Paradigm Shift](http://pdlla.org/posts/portfolio/games/epoch5.html)
- [Beamtimes and Lifetimes: The Card Game](http://pdlla.org/posts/portfolio/games/btlt_tcg.html)
- [Beatdown](http://pdlla.org/posts/hidden/beatdown.html)

### Performance / Sculpture

- [Mushroom #1](http://pdlla.org/posts/portfolio/art/mushrooms/mushroom_throne.html)
- [Mushroom #2](http://pdlla.org/posts/portfolio/art/mushrooms/freshman_mushroom_project.html)
- [Poops Pots and Pottery](http://pdlla.org/posts/portfolio/art/ceramics/poops_pots_and_pottery.html)
- [$2.01/The Master's Tools will Never Dismantle the Master's House](http://pdlla.org/posts/portfolio/art/other/two_dollars_and_one_cents.html)

### Teaching 
- [CTIN 486 Alternative Control Workshop Spring 16](http://pdlla.org/posts/teaching/USC_486_S16.html)