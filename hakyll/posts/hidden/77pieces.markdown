---
title: 77-pieces
date: August 23, 2016
tags: none 
thumbnail: none
gallery: 
---

77-pieces developed clothing simulation software for applications in all levels of the fashion industry. All media on this page is owned by 77-pieces and generated by code I wrote during my stay with them. This page is for resume purposes only.

<iframe width="420" height="315" src="https://www.youtube.com/embed/M7a0pJBl8Qo" frameborder="0" allowfullscreen></iframe>

[![scale700](/images/77pieces.jpg)](/images/77pieces.jpg)