---
title: Beatdown
date: December 8, 2015
tags: games 
thumbnail:  beatdown/
gallery: auto ../images/beatdown
---

<iframe src="https://player.vimeo.com/video/58563773" width="500" height="375" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

A game of generative music, slapstick violence, and large buttons.

Made with Pure Data, Raspberry Pi, Python.

Produced in collaboration with [http://xander-underwhelm.com/projects/BeatDown](Alex Rickett) and [http://hadto.net](David Elliot).

Made at the [http://games.ucla.edu/game/beat-down/](UCLA Game Lab).